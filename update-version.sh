#!/bin/bash

# Get the last commit date
commit_date=$(git log -1 --format=%cd --date=format:%y%m%d)

# Get the first eight digits of the commit hash
commit_hash=$(git log -1 --format=%h)

# Combine the values to form the new version
new_version="${commit_date}-${commit_hash}"

# Print the new version
echo "$new_version" > $CI_PROJECT_DIR/version.env

# Set the new version in the POM.xml
mvn_command="mvn versions:set -DnewVersion=$new_version"


# Execute Maven command
if $mvn_command ; then
    echo "Maven command executed successfully."
else
    echo "Error: Failed to execute Maven command. Check your project setup."
    exit 1
fi
