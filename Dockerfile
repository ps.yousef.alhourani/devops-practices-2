# Use an official OpenJDK runtime as a parent image
FROM openjdk:11
RUN useradd -r myuser

# Set environment variables for H2 profile.
ENV SPRING_PROFILES_ACTIVE=H2 



# Set the working directory to /app
WORKDIR /app

# Copy the packaged JAR file into the container at /app
COPY target/assignment-*.jar /app 

USER myuser

# Define the command to run your application
ENTRYPOINT ["sh", "-c", "java -jar -Dserver.port=8070 -Dspring.profiles.active=h2 assignment-*.jar" ]
